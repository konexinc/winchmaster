import zmq, keyboard, time
from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
from Kinetic import extractPoints
from numpy import *
IP = "10.0.0.105"

context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://"+IP+":8080")
poll = zmq.Poller()
poll.register(socket, zmq.POLLIN)

k = PyKinectRuntime.PyKinectRuntime(PyKinectV2.FrameSourceTypes_Body)
print "Kinect lance"
id = 1
L0 = 3.467
x1 = 0
y1 = 3.8
z1 = 3.17
x2 = 0
y2 = 0
z2 = 3.17

L1=L0
L2=L0
x=0.0
y=1.9
z=0.0
while True :
    xh = 0
    yh = 1.9
    zh = 1.0
    seeBody = False
    if k.has_new_body_frame():
        bs = k.get_last_body_frame()
        tiltrad = arctan(bs.floor_clip_plane.z/bs.floor_clip_plane.y)
        w = bs.floor_clip_plane.w
        print tiltrad*180.0/pi
        if bs is not None:
            for b in bs.bodies:
                if not b.is_tracked:
                    continue
                # get joints positions
                js = b.joints
                kpos = extractPoints(js,tiltrad,w)
                xh=0
                yh=kpos["r_tip"][1]
                zh=kpos["r_tip"][2]
                #print kpos["r_tip"]
    if keyboard.is_pressed("up"):
        z = z+0.01
        L1=((y1-y)**2+(z1-z)**2)**0.5
        L2=((y2-y)**2+(z2-z)**2)**0.5
        req = [{"id":0,"ordre":"pos","param":int(74.0*(L0-L1))},{"id":1,"ordre":"pos","param":int(74.0*(L0-L2))}]
    elif keyboard.is_pressed("down"):
        z = z-0.01
        L1=((y1-y)**2+(z1-z)**2)**0.5
        L2=((y2-y)**2+(z2-z)**2)**0.5
        req = [{"id":0,"ordre":"pos","param":int(74.0*(L0-L1))},{"id":1,"ordre":"pos","param":int(74.0*(L0-L2))}]
    elif keyboard.is_pressed("left"):
        y = y-0.01
        L1=((y1-y)**2+(z1-z)**2)**0.5
        L2=((y2-y)**2+(z2-z)**2)**0.5
        req = [{"id":0,"ordre":"pos","param":int(74.0*(L0-L1))},{"id":1,"ordre":"pos","param":int(74.0*(L0-L2))}]
    elif keyboard.is_pressed("right"):
        y = y+0.01
        L1=((y1-y)**2+(z1-z)**2)**0.5
        L2=((y2-y)**2+(z2-z)**2)**0.5
        req = [{"id":0,"ordre":"pos","param":int(74.0*(L0-L1))},{"id":1,"ordre":"pos","param":int(74.0*(L0-L2))}]
    elif keyboard.is_pressed("r"):
        L1=((y1-y)**2+(z1-z)**2)**0.5
        L2=((y2-y)**2+(z2-z)**2)**0.5
        req = [{"id":0,"ordre":"pos","param":int(74.0*(L0-L1))},{"id":1,"ordre":"pos","param":int(74.0*(L0-L2))}]
    elif keyboard.is_pressed("z"):
        req = [{"id":0,"ordre":"vit","param":0.3},{"id":1,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("s"):
        req = [{"id":0,"ordre":"vit","param":-0.3},{"id":1,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("a"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.3}]
    elif keyboard.is_pressed("q"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":-0.3}]
    elif keyboard.is_pressed("t"):
        req = [{"id":0,"ordre":"raz","param":0.0},{"id":1,"ordre":"raz","param":0.0}]
    elif keyboard.is_pressed("g"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0}]
    else:
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0}]
    socket.send_json(req)
    print x,y,z
    #print req
    expect = True
    while expect:
        socks = dict(poll.poll(100))
        if socks.get(socket) == zmq.POLLIN:
            rep = socket.recv_json()
            expect = False
    time.sleep(0.001)