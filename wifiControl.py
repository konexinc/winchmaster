import zmq

def class wifiControl(object):

    def __init__(self,IP,type):
        self.context = zmq.Context()
        if type == "req":
            self.socket = context.socket(zmq.REQ)
            self.socket.connect("tcp://"+IP+":8080")
        elif type == "rep":
            self.socket = context.socket(zmq.REP)
            self.socket.bind("tcp://0.0.0.0:8080")
        self.poll = zmq.Poller()
        self.poll.register(socket, zmq.POLLIN)
        
    def askRequest(self,req):
        self.socket.send_json(req)
        expect = True
        while expect:
            socks = dict(self.poll.poll(100))
            if socks.get(self.socket) == zmq.POLLIN:
                rep = self.socket.recv_json()
                expect = False
        return rep
        
    def isAsked(self):
        socks = dict(self.poll.poll(100))
        if socks.get(self.socket) == zmq.POLLIN:
            return self.socket.recv_json()
        else:
            return None
            
    def reply(self,rep):
        self.socket.send_json(rep)