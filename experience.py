import numpy as np

def addPos(tableau, donnees):
    tableau = np.append(tableau, donnees, axis = 0)
    return tableau

tableauPos = np.empty((0,6))
    
tableauPos = addPos(tableauPos, [[1.,1.,1.,1.,1.,1.]])
tableauPos = addPos(tableauPos, [[2.,1.,1.,1.,1.,1.]])
print tableauPos

np.save("memoireTableauPos", tableauPos)

a = np.load("memoireTableauPos.npy")
print a