import zmq, keyboard, time
from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
from scipy import ndimage

k = PyKinectRuntime.PyKinectRuntime(PyKinectV2.FrameSourceTypes_Depth)
width = k.depth_frame_desc.Width
height = k.depth_frame_desc.Height
isum = 0.0
jsum = 0.0
nbpoint = 0
time.sleep(2.0)
if k.has_new_depth_frame():
    frame = k.get_last_depth_frame()
    f=frame.reshape((height,width))
    for i in range(height):
        for j in range(width):
            if f[i][j] >4000:
                f[i][j] = 0
    a=ndimage.binary_erosion(f)
    a=ndimage.binary_erosion(a)
    for i in range(height):
        for j in range(width):
            if a[i][j] >0:
                isum = isum+i
                jsum = jsum+j
                nbpoint = nbpoint + 1
    imoy = int(isum/nbpoint)
    jmoy = int(jsum/nbpoint)
    z = f[imoy][jmoy]
    plt.scatter(jmoy, imoy, s=200, marker=(5, 0))
    print imoy,jmoy,z
    plt.imshow(f)
    plt.show()
