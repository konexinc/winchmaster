from rot import RotaryEncoder
from gpiozero import PWMOutputDevice, DigitalOutputDevice, Button
import time, zmq

nMot = 4
pas = [0,0,0,0]
pasc = [0,0,0,0]
tpas = [0,0,0,0]
v = [0.0,0.0,0.0,0.0]
mode = ["vit","vit","vit","vit"]

pinA = [5 , 7,24,11]
pinB = [25 ,8,22,23]

pinPWM = [17,18,3,14]
pinDir = [15,27,2, 4]

pintouch = [13,6,9,10]
#pinEnable = 6

dt = 0.01
K = -0.02
vMax = 0.3
freq = 440

def onCountChange0(pValue):
    global pas, tpas
    i = 0
    tpas[i] = time.time()
    pas[i] = pas[i] + pValue
    
def onCountChange1(pValue):
    global pas, t
    i = 1
    tpas[i] = time.time()
    pas[i] = pas[i] + pValue

def onCountChange2(pValue):
    global pas, t
    i = 2
    tpas[i] = time.time()
    pas[i] = pas[i] + pValue
    
def onCountChange3(pValue):
    global pas, t
    i = 3
    tpas[i] = time.time()
    pas[i] = pas[i] + pValue

    
r = []
for i in range(nMot):
    r.append(RotaryEncoder(pinA[i],pinB[i],pull_up=True))
r[0].when_rotated = onCountChange0
r[1].when_rotated = onCountChange1
r[2].when_rotated = onCountChange2
r[3].when_rotated = onCountChange3

#e = DigitalOutputDevice(pinEnable)

m = []
d = []
to = []
for i in range(nMot):
    m.append(PWMOutputDevice(pinPWM[i],frequency = freq))
    d.append(DigitalOutputDevice(pinDir[i]))
    to.append(Button(pintouch[i], True))

context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.bind("tcp://*:8080")
socket.setsockopt(zmq.SUBSCRIBE,'')
poll = zmq.Poller()
poll.register(socket,zmq.POLLIN)

# enable drivers
#e.on()
for i in range(nMot):
    d[i].on()
    m[i].value = 0.0



while True:
    # sensors
    t = time.time()
    # command
    socks = dict(poll.poll(0))
    if socks.get(socket) == zmq.POLLIN:
        rep = socket.recv_json()
        print rep
        #socket.send_json({'tendu':[to[0].value,to[1].value,to[2].value,to[3].value]})
        for repi in rep:
            id = repi["id"]
            if id <=3:
                if repi["ordre"] == "raz":
                    mode[id] = "vit"
                    pas[id] = repi["param"]
                elif repi["ordre"] == "vit":
                    mode[id] = "vit"
                    v[id] = repi["param"]
                elif repi["ordre"] == "pos":
                    mode[id] = "pos"
                    pasc[id] = repi["param"]
            elif id == 4:
                vMax = repi["Vmax"]
                K = repi["K"]
                freq = repi["freq"]
    for i in range(nMot):
        if mode[i] == "pos":
            v[i] = K*(pas[i]-pasc[i])
            if v[i] > vMax :
                v[i] = vMax
            if v[i] < -vMax:
                v[i] = -vMax
            if abs(v[i])<0.05:
                v[i] =0.0
        if v[i] > 0.0 :
            d[i].on()
            m[i].value = v[i]
        elif v[i] == 0.0:
            m[i].value = 0.0
        elif v[i] < 0.0 and to[i].value == False:
            d[i].off()
            m[i].value = -v[i]
        elif v[i] < 0.0 and to[i].value == True:
            d[i].on()
            m[i].value = 0.0
    print pas,to[0].value,to[1].value,to[2].value,to[3].value
    time.sleep(dt)



