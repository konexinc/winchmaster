import zmq, keyboard, time
from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
from Kinetic import extractPoints
IP = "10.0.0.105"

context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://"+IP+":8080")
poll = zmq.Poller()
poll.register(socket, zmq.POLLIN)

k = PyKinectRuntime.PyKinectRuntime(PyKinectV2.FrameSourceTypes_Body)
print "Kinect lance"
id = 1
L0 = 3.467
x1 = 0
y1 = 3.8
z1 = 3.17
x2 = 0
y2 = 0
z2 = 3.17
while True :
    xh = 0
    yh = 1.9
    zh = 1.0
    seeBody = False
    if k.has_new_body_frame():
        bs = k.get_last_body_frame()
        if bs is not None:
            for b in bs.bodies:
                if not b.is_tracked:
                    continue
                # get joints positions
                js = b.joints
                kpos = extractPoints(js)
                xh=0
                yh=kpos["r_tip"][1]
                zh=kpos["r_tip"][2]
                #print kpos["r_tip"]
    
    req = {"id":id,"ordre":"up","param":0.0}
    if keyboard.is_pressed("u"):
        req = {"id":id,"ordre":"up","param":0.3}
    if keyboard.is_pressed("d"):
        req = {"id":id,"ordre":"down","param":0.3}
    if keyboard.is_pressed("z"):
        req = {"id":id,"ordre":"raz","param":0.0}
    if keyboard.is_pressed("c"):
        req = {"id":id,"ordre":"pos","param":100}
    if keyboard.is_pressed("k"):
        L1 = ((y1-yh)**2+(z1-zh)**2)**0.5
        L2 = ((y2-yh)**2+(z2-zh)**2)**0.5
        #print L0,L1,L2
        if id == 1:
            req = {"id":1,"ordre":"pos","param":int(74.0*(L0-L1))}
            id = 2
        elif id == 2:
            req = {"id":2,"ordre":"pos","param":int(74.0*(L0-L2))}
            id = 1
        
    if keyboard.is_pressed("s"):
        if id == 1:
            id = 2
            time.sleep(0.5)
        elif id == 2:
            id = 1
            time.sleep(0.5)
        print id
    socket.send_json(req)
    #print req
    
    expect = True
    while expect:
        socks = dict(poll.poll(100))
        if socks.get(socket) == zmq.POLLIN:
            rep = socket.recv_json()
            expect = False
    time.sleep(0.001)