import zmq, keyboard, time
from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
from Kinetic import extractPoints
from numpy import *
from numpy.linalg import norm
import random
IP = "10.0.0.103"

context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://"+IP+":8080")
poll = zmq.Poller()
poll.register(socket, zmq.POLLIN)

k = PyKinectRuntime.PyKinectRuntime(PyKinectV2.FrameSourceTypes_Body)
tilt=-2.8870*pi/180.0
w=1.0199375
pan=33.71071*pi/180.0
d=2.66981006
print "Kinect lance"

P0 = array([0.0,0.0,0.0])
P1 = array([ -1.84, -1.78,4.12])
P2 = array([-1.84, 1.78,4.30])
P3 = array([1.84,1.78,4.3])
P4 = array([ 1.84,-1.78,4.12])
L0 = norm(P1)

L1 = L0
L2 = L0
L3 = L0
L4 = L0
M = P0
inc = 0.01
dt = 0.01
tdetect = time.time()
freq = 440
M = array([0.0,0.0,3.0])
while True :
    PH = array([0.0,0.0,3.0])
    seeBody = False
    if k.has_new_body_frame():
        bs = k.get_last_body_frame()
        if bs is not None:
            for b in bs.bodies:
                if not b.is_tracked:
                    continue
                # get joints positions
                js = b.joints
                kpos = extractPoints(js,tilt,w,pan,d)
                PH = kpos["l_tip"]
                PH[2]=3.0
                print PH
    if keyboard.is_pressed("up") or keyboard.is_pressed("down") or keyboard.is_pressed("left") or keyboard.is_pressed("right") or keyboard.is_pressed("k") or keyboard.is_pressed("o") or keyboard.is_pressed("p"):
        if keyboard.is_pressed("up"):
            M[2] = M[2]+inc
            vmax=0.3
        elif keyboard.is_pressed("down"):
            M[2] = M[2]-inc
            vmax=0.3
        elif keyboard.is_pressed("left"):
            M[1] = M[1]-inc
            vmax=0.3
        elif keyboard.is_pressed("right"):
            M[1] = M[1]+inc
            vmax=0.3
        elif keyboard.is_pressed("k"):
            M = PH
            vmax=0.3
        elif keyboard.is_pressed("o"):
            freq_circle = 0.01
            r_circle = 1.5
            M = array([r_circle*sin(2*pi*freq_circle*time.time()),r_circle*cos(2*pi*freq_circle*time.time()),3.0])
            dist = norm(PH-M)
            if dist<0.5:
                tdetect = time.time()
            vmax=0.2
        elif keyboard.is_pressed("p"):
            dist = norm(PH-M)
            vmax=0.5
            print dist
            if dist<0.5:
                tdetect = time.time()
                M = array([random.random()*2.0-1.0,random.random()*2.0-1.0,3.0])
                          
            
        if M[0]>1.9:
            M[0] = 1.9
        if M[0]<-1.9:
            M[0] = -1.9
        if M[1]>1.9:
            M[1] = 1.9
        if M[1]<-1.9:
            M[1] = -1.9
        if M[2]>3.0:
            M[2] = 3.0
        if M[2]<0.0:
            M[2] = 0.0
        
        L1=norm(M-P1)
        L2=norm(M-P2)
        L3=norm(M-P3)
        L4=norm(M-P4)
        #print "long"
        #print L0,L1,L2,L3,L4
        if time.time()-tdetect < 0.1:
            req = [{"id":0,"ordre":"vit","param":0.8},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
        else:
            req = [{"id":0,"ordre":"pos","param":int(74.0*(L0-L1))},{"id":1,"ordre":"pos","param":int(74.0*(L0-L2))},{"id":2,"ordre":"pos","param":int(74.0*(L0-L3))},{"id":3,"ordre":"pos","param":int(74.0*(L0-L4))},{"id":4,"Vmax":vmax,"K":-0.02,"freq":freq}]
        
    elif keyboard.is_pressed("a"):
        req = [{"id":0,"ordre":"vit","param":0.3},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("z"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.3},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("e"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.3},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("r"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.3}]
    elif keyboard.is_pressed("q"):
        req = [{"id":0,"ordre":"vit","param":-0.3},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("s"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":-0.3},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("d"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":-0.3},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("f"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":-0.3}]
    elif keyboard.is_pressed("t"):
        req = [{"id":0,"ordre":"raz","param":0},{"id":1,"ordre":"raz","param":0},{"id":2,"ordre":"raz","param":0},{"id":3,"ordre":"raz","param":0}]
        M = P0
    elif keyboard.is_pressed("g"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("x"):
        freq = freq+100
        if freq<100:
            freq = 100
        print freq
    elif keyboard.is_pressed("w"):
        freq = freq-100
        if freq<100:
            freq = 100
        print freq
    else:
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0},{"id":4,"Vmax":0.3,"K":-0.02,"freq":freq}]
    socket.send_json(req)
    #print "M"
    #print M
    expect = True
    while expect:
        socks = dict(poll.poll(100))
        if socks.get(socket) == zmq.POLLIN:
            rep = socket.recv_json()
            expect = False
    time.sleep(dt)