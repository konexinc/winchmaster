import zmq, keyboard, time
from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
from Kinetic import extractPoints
from numpy import *
from numpy.linalg import norm
IP = "10.0.0.105"

context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://"+IP+":8080")
poll = zmq.Poller()
poll.register(socket, zmq.POLLIN)

k = PyKinectRuntime.PyKinectRuntime(PyKinectV2.FrameSourceTypes_Body)
tilt=-0.783683*pi/180.0
w=1.0199375
pan=-39.726274*pi/180.0
d=2.66981006
print "Kinect lance"

P0 = array([0.0,0.0,0.0])
P1 = array([ 1.9, 1.9,3.17])
P2 = array([-1.9, 1.9,3.17])
P3 = array([-1.9,-1.9,3.17])
P4 = array([ 1.9,-1.9,3.17])
L0 = norm(P1)

L1 = L0
L2 = L0
L3 = L0
L4 = L0
M = P0
inc = 0.01
dt = 0.01
freq = 0.1
while True :
    PH = array([0.0,0.0,3.0])
    seeBody = False
    if k.has_new_body_frame():
        bs = k.get_last_body_frame()
        if bs is not None:
            for b in bs.bodies:
                if not b.is_tracked:
                    continue
                # get joints positions
                js = b.joints
                kpos = extractPoints(js,tilt,w,pan,d)
                PH = kpos["l_tip"]
                PH[2]=3.0
    if keyboard.is_pressed("up") or keyboard.is_pressed("down") or keyboard.is_pressed("left") or keyboard.is_pressed("right") or keyboard.is_pressed("k") or keyboard.is_pressed("o"):
        if keyboard.is_pressed("up"):
            M[2] = M[2]+inc
        elif keyboard.is_pressed("down"):
            M[2] = M[2]-inc
        elif keyboard.is_pressed("left"):
            M[1] = M[1]-inc
        elif keyboard.is_pressed("right"):
            M[1] = M[1]+inc
        elif keyboard.is_pressed("k"):
            M = PH
        elif keyboard.is_pressed("o"):
            M = array([1.0*sin(2*pi*freq*time.time()),1.0*cos(2*pi*freq*time.time()),1.5])
        if M[0]>1.9:
            M[0] = 1.9
        if M[0]<-1.9:
            M[0] = -1.9
        if M[1]>1.9:
            M[1] = 1.9
        if M[1]<-1.9:
            M[1] = -1.9
        if M[2]>3.0:
            M[2] = 3.0
        if M[2]<0.0:
            M[2] = 0.0
        
        L1=norm(M-P1)
        L2=norm(M-P2)
        L3=norm(M-P3)
        L4=norm(M-P4)
        print "long"
        print L0,L1,L2,L3,L4
        req = [{"id":0,"ordre":"pos","param":int(74.0*(L0-L1))},{"id":1,"ordre":"pos","param":int(74.0*(L0-L2))},{"id":2,"ordre":"pos","param":int(74.0*(L0-L3))},{"id":3,"ordre":"pos","param":int(74.0*(L0-L4))}]
        
    elif keyboard.is_pressed("a"):
        req = [{"id":0,"ordre":"vit","param":0.3},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("z"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.3},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("e"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.3},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("r"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.3}]
    elif keyboard.is_pressed("q"):
        req = [{"id":0,"ordre":"vit","param":-0.3},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("s"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":-0.3},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("d"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":-0.3},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("f"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":-0.3}]
    elif keyboard.is_pressed("t"):
        req = [{"id":0,"ordre":"raz","param":0.0},{"id":1,"ordre":"raz","param":0.0},{"id":2,"ordre":"raz","param":0.0},{"id":3,"ordre":"raz","param":0.0}]
        M = P0
    elif keyboard.is_pressed("g"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
    else:
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
    socket.send_json(req)
    print "M"
    print M
    expect = True
    while expect:
        socks = dict(poll.poll(100))
        if socks.get(socket) == zmq.POLLIN:
            rep = socket.recv_json()
            expect = False
    time.sleep(dt)