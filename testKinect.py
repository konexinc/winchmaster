# -*- coding: utf-8 -*-
import time
from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
from Kinetic import extractPoints
from numpy import *
import pyttsx



k = PyKinectRuntime.PyKinectRuntime(PyKinectV2.FrameSourceTypes_Body)
print "Kinect lance"
e = pyttsx.init()

tilt=5.5789*pi/180.0
w=1.17
pan=32.99*pi/180.0
d=2.224

calib = True
while calib :
    seeBody = False
    if k.has_new_body_frame():
        bs = k.get_last_body_frame()
        #tilt = arctan(bs.floor_clip_plane.z/bs.floor_clip_plane.y)
        #w = bs.floor_clip_plane.w
        if bs is not None:
            for b in bs.bodies:
                if not b.is_tracked:
                    continue
                # get joints positions
                js = b.joints
                kpos = extractPoints(js,tilt,w,pan,d)
                print kpos["l_tip"]
                e.say("position ixe "+str(int(kpos["l_tip"][0]*100))+" . igrec "+str(int(kpos["l_tip"][1]*100))+" . zedeu "+str(int(kpos["l_tip"][2]*100)))
                e.runAndWait()