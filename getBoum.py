import pyaudio
import numpy as np
import time

RATE = 44100
CHUNK = int(RATE/20) # RATE / number of updates per second

if __name__=="__main__":
    p=pyaudio.PyAudio()
    stream=p.open(format=pyaudio.paInt16,channels=1,rate=RATE,input=True,
                  frames_per_buffer=CHUNK)
    for i in range(int(20*RATE/CHUNK)): #do this for 10 seconds
        data = np.fromstring(stream.read(CHUNK),dtype=np.int16)
        if max(data)>1000:
            print "BOUM"
    stream.stop_stream()
    stream.close()
    p.terminate()