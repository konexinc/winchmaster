import zmq, keyboard, time
from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
from scipy import ndimage
IP = "10.0.0.103"

def addPos(tableau, donnees):
    tableau = np.append(tableau, donnees, axis = 0)
    return tableau

#init de l'environnement
k = PyKinectRuntime.PyKinectRuntime(PyKinectV2.FrameSourceTypes_Depth)
width = k.depth_frame_desc.Width
height = k.depth_frame_desc.Height
# init du contexte zmq (treuil)
context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://"+IP+":8080")
poll = zmq.Poller()
poll.register(socket, zmq.POLLIN)

#initialisation des parametres géométriques
P0 = array([0.0,0.0,0.0])
P1 = array([ -1.84, -1.78,4.12])
P2 = array([-1.84, 1.78,4.30])
P3 = array([1.84,1.78,4.3])
P4 = array([ 1.84,-1.78,4.12])
L0 = norm(P1)

# initialisation de l'environnement d'apprentissage
tableauPos = np.empty((0,6))

# point de commande / Input de l'expérimentation
M = array([0,0,3])

time.sleep(5)
for i in range(100): 
    # On pilote la machine sur le point de commande m
    L1=norm(M-P1)
    L2=norm(M-P2)
    L3=norm(M-P3)
    L4=norm(M-P4)
    # definition des longueurs de cable exprimées en pas
    req = [{"id":0,"ordre":"pos","param":int(74.0*(L0-L1))},{"id":1,"ordre":"pos","param":int(74.0*(L0-L2))},{"id":2,"ordre":"pos","param":int(74.0*(L0-L3))},{"id":3,"ordre":"pos","param":int(74.0*(L0-L4))},{"id":4,"Vmax":vmax,"K":-0.02,"freq":freq}]
    # envoi
    socket.send_json(req)
    # boucle d'attente de la réponse
    expect = True
    while expect:
        socks = dict(poll.poll(100))
        if socks.get(socket) == zmq.POLLIN:
            rep = socket.recv_json()
            expect = False
            
    # On attend qu'il arrive sur le point
    time.sleep(3)
    
    # On prend la photo
    isum = 0.0
    jsum = 0.0
    nbpoint = 0
    frame = k.get_last_depth_frame()
    f=frame.reshape((height,width))
    for i in range(height):
        for j in range(width):
            if f[i][j] >4000:
                f[i][j] = 0
    a=ndimage.binary_erosion(f)
    a=ndimage.binary_erosion(a)
    for i in range(height):
        for j in range(width):
            if a[i][j] >0:
                isum = isum+i
                jsum = jsum+j
                nbpoint = nbpoint + 1
    imoy = int(isum/nbpoint)
    jmoy = int(jsum/nbpoint)
    z = f[imoy][jmoy]
    plt.scatter(jmoy, imoy, s=200, marker=(5, 0))
    plt.imshow(f)
    plt.show()
    
    # On stocke les données dans le tableau
    tableauPos = addPos(tableauPos, np.append(M,imoy,jmoy,z))
    print tableauPos
    np.save("memoireTableauPos", tableauPos)
    
    # On change la position de M
    M = M+np.random.random(3)-0.5
    M[2]=3.
    