import zmq, keyboard, time
from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
from Kinetic import extractPoints
from numpy import *
from numpy.linalg import norm
from scipy import ndimage
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import random
import pyaudio
IP = "10.0.0.102"
RATE = 44100
CHUNK = int(RATE/20) # RATE / number of updates per second

context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://"+IP+":8080")
poll = zmq.Poller()
poll.register(socket, zmq.POLLIN)

p=pyaudio.PyAudio()
stream=p.open(format=pyaudio.paInt16,channels=1,rate=RATE,input=True,frames_per_buffer=CHUNK)
k = PyKinectRuntime.PyKinectRuntime(PyKinectV2.FrameSourceTypes_Body | PyKinectV2.FrameSourceTypes_Depth)
width = k.depth_frame_desc.Width
height = k.depth_frame_desc.Height
tilt=-1.635*pi/180.0
w=1.47
# pan recalage = 33 deg
pan=-29.111*pi/180.0
d=2.139
print "Kinect lance"
#gain des treuil
pasParMetre1 = 212.0
pasParMetre2 = 212.0
pasParMetre3 = 212.0
pasParMetre4 = 212.0
speed_treuil = 0.3
zmax = 2.0

P0 = array([0.0,0.0,0.0])
P1 = array([-1.10, -1.20,2.70])
P2 = array([ 1.10, -1.20,2.70])
P3 = array([ 1.10,  1.20,2.70])
P4 = array([-1.10,  1.20,2.70])
L10 = norm(P1)
L20 = norm(P2)
L30 = norm(P3)
L40 = norm(P4)

PosCreat = array([0.0,0.0,0.0])
id_interdites = []

L1 = L10
L2 = L20
L3 = L30
L4 = L40
M = P0
inc = 0.01
dt = 0.01
tdetect = time.time()
freq = 440
isAuto = False
emotion = "love"
M = array([0.0,0.0,zmax])
bdetendu = [False,False,False,False]

while True :
    now = time.clock()
    PH = [array([0.0,0.0,zmax])]
    PM = []
    seeBody = False
    if k.has_new_body_frame():
        bs = k.get_last_body_frame()
        if bs is not None:
            for b in bs.bodies:
                if b.is_tracked and (not b.tracking_id in id_interdites):
                    js = b.joints
                    kpos = extractPoints(js,tilt,w,pan,d)
                    if (0.5*kpos["r_toe"][2]+0.5*kpos["l_toe"][2])<0.5:
                        if kpos["head"][0]<1.10 and kpos["head"][0]>-1.10:
                            if kpos["head"][1]<1.20 and kpos["head"][1]>-1.20:
                                t = kpos["head"]
                                PH.append(t)
                                u = kpos["l_tip"]
                                u[2] = zmax
                                v = kpos["r_tip"]
                                v[2] = zmax
                                PM.append(u)
                                PM.append(v)
    #get boum
    isboum = 0
    data = fromstring(stream.read(CHUNK),dtype=int16)
    if max(data)>10000:
        isboum = 1
        print "BOUM"
        
    if keyboard.is_pressed("up") or keyboard.is_pressed("down") or keyboard.is_pressed("left") or keyboard.is_pressed("right") or keyboard.is_pressed("k") or keyboard.is_pressed("o") or keyboard.is_pressed("p") or keyboard.is_pressed("n") or isAuto==True:
        stop = 0
        if keyboard.is_pressed("up"):
            M[2] = M[2]+inc
            vmax=0.5
        elif keyboard.is_pressed("down"):
            M[2] = M[2]-inc
            vmax=0.5
        elif keyboard.is_pressed("left"):
            M[1] = M[1]-inc
            vmax=0.5
        elif keyboard.is_pressed("right"):
            M[1] = M[1]+inc
            vmax=0.5
        elif keyboard.is_pressed("k"):
            M = array([0.0,0.0,zmax])
            for head in PH:
                if head[2]<M[2]:
                    M = head
            M[2]=zmax
            vmax=0.3
        elif keyboard.is_pressed("p"):
            for hand in PM:
                dist = norm(hand-M)
                vmax=0.5
                print dist
                if dist<1.0:
                    tdetect = time.time()
                    M = array([random.random()*2.0-1.0,random.random()*2.0-1.0,zmax])
        elif keyboard.is_pressed("n"):
            if isAuto == True:
                isAuto = False
                print "mode Auto desactive"
                time.sleep(0.5)
            else:
                isAuto = True
                emotion = "sleep"
                print "mode Auto active"
                time.sleep(0.5)
        if isAuto:
            print emotion
            if emotion == "sleep":
                stop = 1
                if len(PH)>1:
                    emotion = "love"
                if isboum:
                    t0 = now
                    emotion = "fear"
            elif emotion == "love":
                stop=0
                M = array([0.0,0.0,zmax])
                if len(PH)>1:
                    t0 = now
                if now-t0>5.0:
                    emotion = "sleep"
                for head in PH:
                    if head[2]<M[2]:
                        M = head
                #M[2]=zmax
                vmax=0.3
                if isboum:
                    emotion = "fear"
                    t0 = now
            elif emotion == "fear":
                stop=0
                for hand in PM:
                    dist = norm(hand-M)
                    vmax=0.4
                    if dist<0.5 or isboum:
                        tdetect = time.time()
                        M = array([random.random()*2.0-1.0,random.random()*2.0-1.0,zmax])
                if now-t0>10:
                    emotion = "love"
                if isboum:
                    t0 = now
        
        if M[0]>1.10:
            M[0] = 1.10
        if M[0]<-1.10:
            M[0] = -1.10
        if M[1]>1.20:
            M[1] = 1.20
        if M[1]<-1.20:
            M[1] = -1.20
        if M[2]>zmax:
            M[2] = zmax
        if M[2]<0.0:
            M[2] = 0.0
        
        L1=norm(M-P1)
        L2=norm(M-P2)
        L3=norm(M-P3)
        L4=norm(M-P4)
        #print "long"
        #print L1,L10
        #print L2,L20
        #print L3,L30
        #print L4,L40
        print M
        if stop == 0:
            if time.time()-tdetect < 0.1:
                req = [{"id":0,"ordre":"vit","param":0.5},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
            else:
                req = [{"id":0,"ordre":"pos","param":int(pasParMetre1*(L10-L1))},{"id":1,"ordre":"pos","param":int(pasParMetre2*(L20-L2))},{"id":2,"ordre":"pos","param":int(pasParMetre3*(L30-L3))},{"id":3,"ordre":"pos","param":int(pasParMetre4*(L40-L4))},{"id":4,"Vmax":vmax,"K":-0.02,"freq":freq}]
        else:
            req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0},{"id":4,"Vmax":0.3,"K":-0.02,"freq":freq}]
    elif keyboard.is_pressed("c"):
        if k.has_new_depth_frame():
            isum = 0.0
            jsum = 0.0
            zsum = 0.0
            nbpoint = 0
            frame = k.get_last_depth_frame()
            f=frame.reshape((height,width))
            for i in range(height):
                for j in range(width):
                    if f[i][j] >2500:
                        f[i][j] = 0
            a=ndimage.binary_erosion(f)
            a=ndimage.binary_erosion(a)
            for i in range(height):
                for j in range(width):
                    if a[i][j] >0:
                        isum = isum+i
                        jsum = jsum+j
                        nbpoint = nbpoint + 1
                        zsum = zsum + f[i][j]
            imoy = int(isum/nbpoint)
            jmoy = int(jsum/nbpoint)
            zmoy = int(zsum/nbpoint)

            Z = w*1000.0+zmoy*sin((30-imoy*(60.0/424.0))*pi/180)
            prof = zmoy*cos((-35.5+jmoy*(70.6/512.0))*pi/180.0)
            dy = zmoy*sin((-35.5+jmoy*(70.6/512.0))*pi/180.0)

            panct = -(pi-pan)

            KMx = prof*cos(panct)-dy*sin(panct)
            KMy = prof*sin(panct)+dy*cos(panct)

            OKx = -d*1000*cos(-panct)
            OKy = d*1000*sin(-panct)

            OMx=OKx+KMx
            OMy=OKy+KMy
            print OMx,OMy,Z
            PosCreat = array([OMx/1000.,OMy/1000.,Z/1000.])
            for b in bs.bodies:
                if b.is_tracked:
                    js = b.joints
                    kpos = extractPoints(js,tilt,w,pan,d)
                    t = kpos["spine_base"]
                    if norm(t-PosCreat)<0.5:
                        id_interdites.append(b.tracking_id)
            #computation of new lengths
            L1n=norm(PosCreat-P1)
            L2n=norm(PosCreat-P2)
            L3n=norm(PosCreat-P3)
            L4n=norm(PosCreat-P4)
            req = [{"id":0,"ordre":"raz","param":int(pasParMetre1*(L10-L1n))},{"id":1,"ordre":"raz","param":int(pasParMetre2*(L20-L2n))},{"id":2,"ordre":"raz","param":int(pasParMetre3*(L30-L3n))},{"id":3,"ordre":"raz","param":int(pasParMetre4*(L40-L4n))}]
            print L1n,L2n,L3n,L4n
            M=PosCreat
            print M
            plt.scatter(jmoy, imoy, s=200, marker=(5, 0))
            plt.imshow(f)
            plt.show()
            
    
    elif keyboard.is_pressed("a"):
        req = [{"id":0,"ordre":"vit","param":speed_treuil},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("z"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":speed_treuil},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("e"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":speed_treuil},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("r"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":speed_treuil}]
    elif keyboard.is_pressed("q"):
        req = [{"id":0,"ordre":"vit","param":-speed_treuil},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("s"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":-speed_treuil},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("d"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":-speed_treuil},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("f"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":-speed_treuil}]
    elif keyboard.is_pressed("t"):
        req = [{"id":0,"ordre":"raz","param":0},{"id":1,"ordre":"raz","param":0},{"id":2,"ordre":"raz","param":0},{"id":3,"ordre":"raz","param":0}]
        M = P0
    elif keyboard.is_pressed("g"):
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0}]
    elif keyboard.is_pressed("x"):
        freq = freq+100
        if freq<100:
            freq = 100
        print freq
    elif keyboard.is_pressed("w"):
        freq = freq-100
        if freq<100:
            freq = 100
        print freq
    elif keyboard.is_pressed("y"):
        req = [{"id":0,"ordre":"vit","param":speed_treuil*bdetendu[0]},{"id":1,"ordre":"vit","param":speed_treuil*bdetendu[1]},{"id":2,"ordre":"vit","param":speed_treuil*bdetendu[2]},{"id":3,"ordre":"vit","param":speed_treuil*bdetendu[3]}]
    else:
        req = [{"id":0,"ordre":"vit","param":0.0},{"id":1,"ordre":"vit","param":0.0},{"id":2,"ordre":"vit","param":0.0},{"id":3,"ordre":"vit","param":0.0},{"id":4,"Vmax":0.3,"K":-0.02,"freq":freq}]
    socket.send_json(req)
    expect = True
    while expect:
        socks = dict(poll.poll(100))
        if socks.get(socket) == zmq.POLLIN:
            rep = socket.recv_json()
            bdetendu = rep['tendu'] 
            expect = False
    time.sleep(dt)
stream.stop_stream()
stream.close()
p.terminate()